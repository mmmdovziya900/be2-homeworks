package hw.hw7;

public class Fish extends Pet {
    private Species species = Species.FISH;
    ;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Fish(String nickname) {
        this.nickname = nickname;
    }

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Fish() {
        this.habits = new String[0];
    }

    @Override
    public void respond() {
        System.out.println("I am wild");
    }

    @Override
    public void foul() {
        super.foul();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Species getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Finalize method called in Pet class");
        super.finalize();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Fish))
            return false;
        if (obj == this)
            return true;
        return this.getTrickLevel() == ((Fish) obj).getTrickLevel();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public String toString() {
        String habit = "";
        if (habits.length != 0) {
            for (int i = 0; i < habits.length; i++) {
                String c = i + 1 < habits.length ? ", " : "";
                habit += habits[i] + c;
            }
            String s = species.name() + "{nickname='" + nickname + "', age=" + age + ", trickLevel=" + trickLevel + ", habits=[" + habit + "]}";
            return s;
        }
        return "";
    }
}
