package hw.hw7;

final class Man extends Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;
    private Family family;

    public Man(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man() {
        super();
    }

    public void repairCar() {
        System.out.println(this.name + " is repairing his car");
    }

    @Override
    public void greetPet() {
        System.out.println("Hello " + this.name + ". My name is " + family.getPet().getNickname());
    }

}
