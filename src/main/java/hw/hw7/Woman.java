package hw.hw7;

final class Woman extends Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;
    private Family family;

    public Woman(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman() {
        super();
    }

    public void makeUp() {
        System.out.println(this.name + " is doing make-up");
    }

    @Override
    public void greetPet() {
        System.out.println("Hello " + this.name + ". Nice to meet you . My name is " + family.getPet().getNickname());
    }
}