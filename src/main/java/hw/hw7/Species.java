package hw.hw7;

public enum Species {
    DOG, DOMESTIC_CAT, FISH, ROBO_CAT, UNKNOWN
}
