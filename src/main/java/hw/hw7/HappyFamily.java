package hw.hw7;

public class HappyFamily {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        schedule[0][0] = Day.SUNDAY.name();
        schedule[0][1] = "do home work";
        schedule[1][0] = Day.MONDAY.name();
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = Day.TUESDAY.name();
        schedule[2][1] = "go in for sport";
        schedule[3][0] = Day.WEDNESDAY.name();
        schedule[3][1] = "clean up home";
        schedule[4][0] = Day.THURSDAY.name();
        schedule[4][1] = "meet friend";
        schedule[5][0] = Day.FRIDAY.name();
        schedule[5][1] = "go to theatre";
        schedule[6][0] = Day.SATURDAY.name();
        schedule[6][1] = "you are free today, do what you want";
        String[] habits = {"Dance", "Jump"};

        Dog pet = new Dog("Molly", 5, 86, habits);

        System.out.println(pet);

        Human mother = new Human("July", "King", 1955, 75, schedule);
        Human father = new Human("Brad", "Pit", 1946, 90, schedule);
        Human child = new Human("Stephen", "King", 1989, 75, schedule);
        Family family = new Family(mother, father);
        family.setPet(pet);

        System.out.println(father.equals(mother));
        family.addChild(child);
        family.addChild(child);
        System.out.println(family.deleteChild(child));
        family.setPet(pet);
        family.getPet().foul();
        family.getPet().respond();
        family.getPet().eat();

        System.out.println(family.toString());
        System.out.println(family.countFamily());
        System.out.println(father.toString());

        pet.eat();
        pet.respond();
        pet.foul();
        for (int i = 0; i < 1000000; i++) {
            new Human();
            if (i > 10000) {
                System.gc();
            }
        }
        System.out.println(pet.toString());
    }

    public enum Day {
        MONDAY, TUESDAY,
        WEDNESDAY, THURSDAY,
        FRIDAY, SATURDAY, SUNDAY
    }
}
