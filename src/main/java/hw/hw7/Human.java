package hw.hw7;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;
    private Family family;

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = new String[0][0];
    }

    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;

    }

    public Human() {

    }

    public void greetPet() {
        System.out.println("Hello, " + family.getPet().getNickname());
    }

    public void describePet() {
        System.out.println("I have a " + family.getPet().getSpecies() + ", he is " + family.getPet().getAge() + " years old, " + (family.getPet().getTrickLevel() > 50 ? "he is very sly" : "he is almost not sly"));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Finalize method called in Human class");
        super.finalize();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Human))
            return false;
        if (obj == this)
            return true;
        return this.getName() == ((Human) obj).getName() && this.getSurname() == ((Human) obj).getSurname();
    }

    @Override
    public String toString() {
        String sh = "";
        for (int i = 0; i < schedule.length; i++) {
            sh += "[" + schedule[i][0] + ", " + schedule[i][1] + "]" + (i + 1 < schedule.length ? ", " : "");
        }
        String s = "Human{name='" + name + "', surname='" + surname + "', year=" + year + ", iq=" + iq + ", schedule=[" + sh + "]}";
        return s;
    }

}
