package hw.hw12.controller;


import hw.hw12.entity.*;
import hw.hw12.service.FamilyService;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TestController {
    private final FamilyService service;

    public TestController(FamilyService service) {
        this.service = service;
    }

    public void load() {
        Map<Enum, String> schedule = new HashMap<>();
        schedule.put(Day.SUNDAY, "do home work");
        schedule.put(Day.MONDAY, "go to courses; watch a film");
        schedule.put(Day.TUESDAY, "go in for sport");
        schedule.put(Day.WEDNESDAY, "clean up home");
        schedule.put(Day.THURSDAY, "meet friend");
        schedule.put(Day.FRIDAY, "go to theatre");
        schedule.put(Day.SATURDAY, "you are free today, do what you want");
        Set<String> habits = new HashSet<>();
        habits.add("Jump");
        habits.add("Dance");
        Pet dog = new Dog("Molly", 5, 86, habits);
        Pet fish = new Fish("Memo", 3, 86, habits);
        Pet domesticCat = new DomesticCat("Max", 3, 86, habits);
        Pet cat = new RoboCat("Sam", 3, 86, habits);

        Human mother = new Woman("July", "King", "20/03/1980", 75);
        Human father = new Man("Brad", "Pit", "20/03/1980", 90);
        Human child = new Man("Stephen", "King", "20/03/2000", 75);
        Human man = new Human("Leanardo", "King", "20/03/1975", 75);
        Human woman = new Human("Angelina", "King", "20/03/1985", 75);
        Family family = new Family(man, woman);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(fish);
        pets.add(domesticCat);
        pets.add(cat);
        service.createNewFamily(father, mother);
        service.adoptChild(family, child);
        service.addPet(0, dog);
        service.bornChild(family, "John", "");
    }

    public enum Day {
        MONDAY, TUESDAY,
        WEDNESDAY, THURSDAY,
        FRIDAY, SATURDAY, SUNDAY
    }
}
