package hw.hw12.controller;

import hw.hw12.EditMenu;
import hw.hw12.console.Console;
import hw.hw12.entity.Family;
import hw.hw12.entity.Human;
import hw.hw12.entity.Pet;
import hw.hw12.service.FamilyService;

import java.util.List;
import java.util.StringJoiner;

public class FamilyController {
    private final FamilyService familyService;
    private final Console console;

    public FamilyController(Console console, FamilyService familyService) {
        this.familyService = familyService;
        this.console=console;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan() {
        console.printLn("ENTER NUMBER TO SEE FAMILIES WHICH MEMBERS IS BIGGER THAN THIS NUMBER :");
        int numOfPeople = checkInputIsInteger(console.readLn());
        return familyService.getFamiliesBiggerThan(numOfPeople);
    }

    public List<Family> getFamiliesLessThan() {
        console.printLn("ENTER NUMBER TO SEE FAMILIES WHICH MEMBERS IS LESS THAN THIS NUMBER :");
        int numOfPeople = checkInputIsInteger(console.readLn());
        return familyService.getFamiliesLessThan(numOfPeople);
    }

    public int countFamiliesWithMemberNumber() {
        return familyService.countFamiliesWithMemberNumber();
    }

    public void createNewFamily() {
        console.printLn("ENTER MOTHER'S NAME: ");
        String motherName = console.readLn();
        console.printLn("ENTER MOTHER'S LAST NAME: ");
        String motherLastName = console.readLn();
        console.printLn("ENTER MOTHER'S BIRTH YEAR(IN FORMAT OF 'YYYY'): ");
        String motherBirthYear = checkInputIsInteger(console.readLn())+"";
        console.printLn("ENTER MOTHER'S BIRTH MONTH(IN FORMAT OF 'MM'): ");
        String motherBirthMonth = checkInputIsInteger(console.readLn())+"";
        console.printLn("ENTER MOTHER'S BIRTH DAY(IN FORMAT OF 'DD': ");
        String motherBirthDay = checkInputIsInteger(console.readLn())+"";
        StringJoiner motherBirthDate = new StringJoiner("/");
        motherBirthDate.add(motherBirthDay).add(motherBirthMonth).add(motherBirthYear);
        console.printLn("ENTER MOTHER'S IQ: ");
        int mIq = checkInputIsInteger(console.readLn());
        console.printLn("ENTER FATHER'S NAME: ");
        String fatherName = console.readLn();
        console.printLn("ENTER FATHER'S LAST NAME: ");
        String fatherLastName = console.readLn();
        console.printLn("ENTER FATHER'S BIRTH YEAR(IN FORMAT OF 'YYYY': ");
        String fatherBirthYear = checkInputIsInteger(console.readLn())+"";
        console.printLn("ENTER FATHER'S BIRTH MONTH(IN FORMAT OF 'MM': ");
        String fatherBirthMonth = checkInputIsInteger(console.readLn())+"";
        console.printLn("ENTER FATHER'S BIRTH DAY(IN FORMAT OF 'DD': ");
        String fatherBirthDay = checkInputIsInteger(console.readLn())+"";
        StringJoiner fatherBirthDate = new StringJoiner("/");
        fatherBirthDate.add(fatherBirthDay).add(fatherBirthMonth).add(fatherBirthYear);
        console.printLn("ENTER FATHER'S IQ: ");
        int fIq = checkInputIsInteger(console.readLn());
        familyService.createNewFamily(new Human(motherName,motherLastName,motherBirthDate.toString(),mIq), new Human(fatherName,fatherLastName,fatherBirthDate.toString(),fIq));
    }

    public boolean deleteFamilyByIndex() {
        console.printLn("ENTER FAMILY INDEX : ");
        int index = checkInputIsInteger(console.readLn());
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild() {
        console.printLn("ENTER AN INDEX OF FAMILY:");
        int index = checkInputIsInteger(console.readLn());
        Family family = getFamilyById(index);
        console.printLn("ENTER A NAME OF BOY (IF YOU DON'T WANT ,JUST TAP ON ENTER): ");
        String nameOfBoy = console.readLn();
        console.printLn("ENTER A NAME OF GIRL (IF YOU DON'T WANT ,JUST TAP ON ENTER): ");
        String nameOfGirl = console.readLn();
        return familyService.bornChild(family, nameOfBoy, nameOfGirl);
    }

    public Family adoptChild() {
        console.printLn("ENTER AN INDEX OF FAMILY:");
        int index = checkInputIsInteger(console.readLn());
        Family family = getFamilyById(index);
        console.printLn("ENTER CHILD'S NAME: ");
        String childName = console.readLn();
        console.printLn("ENTER CHILD'S LAST NAME: ");
        String childLastName = console.readLn();
        console.printLn("ENTER CHILD'S BIRTH YEAR(IN FORMAT OF 'YYYY'): ");
        String childBirthYear = checkInputIsInteger(console.readLn())+"";
        console.printLn("ENTER CHILD'S BIRTH MONTH(IN FORMAT OF 'MM'): ");
        String childBirthMonth = checkInputIsInteger(console.readLn())+"";
        console.printLn("ENTER CHILD'S BIRTH DAY(IN FORMAT OF 'DD'): ");
        String childBirtDay = checkInputIsInteger(console.readLn())+"";
        StringJoiner childBirthDate = new StringJoiner("/");
        childBirthDate.add(childBirtDay).add(childBirthMonth).add(childBirthYear);
        console.printLn("ENTER CHILD'S IQ: ");
        int childIq = checkInputIsInteger(console.readLn());
        return familyService.adoptChild(family, new Human(childName,childLastName,childBirthDate.toString(),childIq));
    }

    public void deleteAllChildrenOlderThan() {
        console.printLn("ENTER AN AGE: ");
        int age = checkInputIsInteger(console.readLn());
        familyService.deleteAllChildrenOlderThan(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public List<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }

    public void editFamily() {
        EditMenu em = new EditMenu();
        console.printLn(em.show());
        console.printLn("SELECT ITEM 1-3");
        String in = console.readLn();
        switch (in){
            case "1":
                bornChild();
                break;
            case "2":
                adoptChild();
                break;
            case "3":
                break;
            default:
                console.printLn("INVALID INPUT");
        }
    }

    private int checkInputIsInteger(String input) {
        int id ;
        while (true) {
            try {
                id = Integer.parseInt(input);
                break;
            } catch (Exception e) {
                console.printLn("PLEASE ENTER A RIGHT INPUT : ");
                input = console.readLn();
            }
        }
        return id;
    }

    public void countWithNumberOfMember() {
        console.printLn("ENTER THE NUMBER OF MEMBERS: ");
        int num = checkInputIsInteger(console.readLn());
         familyService.countWithNumberOfMember(num);
    }
}
