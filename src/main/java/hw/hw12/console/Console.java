package hw.hw12.console;

public interface Console {
    void printLn(Object s);

    String readLn();
}
