package hw.hw12;

public class Menu {

    public String show() {
        StringBuilder sb = new StringBuilder();
        String line = "----------------------------------------------------------------------------------------------------|\n";
        sb.append("=====================================================================================================\n")
                .append("|                                       HAPPY FAMILY                                                |\n")
                .append("=====================================================================================================\n")
                .append(pad("1. FILL WITH TEST DATA (CREATE SEVERAL FAMILIES AND SAVE THEM IN DATABASE)  "))
                .append(line)
                .append(pad("2. DISPLAY ENTIRE LIST OF FAMILIES"))
                .append(line)
                .append(pad("3. DISPLAY A LIST OF FAMILIES WHERE NUMBER OF PEOPLE GREATER THAN: "))
                .append(line)
                .append(pad("4. DISPLAY A LIST OF FAMILIES WHERE NUMBER OF PEOPLE LESS THAN: "))
                .append(line)
                .append(pad("5. CALCULATE THE NUMBER OF FAMILIES WHERE NUMBER OF MEMBERS IS:  "))
                .append(line)
                .append(pad("6. CREATE A NEW FAMILY   "))
                .append(line)
                .append(pad("7. DELETE FAMILY  "))
                .append(line)
                .append(pad("8. EDIT FAMILY "))
                .append(line)
                .append(pad("9. REMOVE ALL CHILDREN WHOSE AGE IS OVER THAN :   "))
                .append(line)
                .append(pad("10. EXIT  "))
                .append(line);

        return sb.toString();
    }

    private String pad(String str) {
        return String.format("|" + "%-" + 99 + "s" + "|" + "\n", str);
    }

}