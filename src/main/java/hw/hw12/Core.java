package hw.hw12;

import hw.hw12.console.Console;
import hw.hw12.controller.FamilyController;
import hw.hw12.controller.TestController;
import hw.hw12.dao.CollectionFamilyDao;
import hw.hw12.service.FamilyService;



public class Core {
    private final Console console;
    private final FamilyController familyController;
    private final  Menu menu;
    private final Parser parser;
    private final TestController tf;

    public Core(Console console) {
        this.parser=new Parser();
        this.menu = new Menu();
        this.console=console;
        FamilyService service = new FamilyService(console, new CollectionFamilyDao());
        this.familyController = new FamilyController(console,service);
        this.tf= new TestController(service);

    }

    public void run() {

        boolean cont = true;
        while (cont) {
            console.printLn("\n" + menu.show());
            console.printLn("SELECT A MENU ITEM:");
            String line = console.readLn();
            Command user_input = parser.parse(line);
            switch (user_input) {
                case TEST:
                    tf.load();
                    break;
                case DISPLAY_FAMILIES:
                    familyController.displayAllFamilies();
                    break;
                case DISPLAY_FAMILIES_MORE_THAN:
                    familyController.getFamiliesBiggerThan();
                    break;
                case DISPLAY_FAMILIES_lESS_THAN:
                    familyController.getFamiliesLessThan();
                    break;
                case COUNT_FAMILIES_OF_NUMBER:
                    familyController.countWithNumberOfMember();
                    break;
                case ADD_FAMILY:
                    familyController.createNewFamily();
                    break;
                case DELETE_FAMILY:
                    familyController.deleteFamilyByIndex();
                    break;
                case EDIT_FAMILY:
                    familyController.editFamily();
                    break;
                case REMOVE_ALL_CHILDREN_OVER_THAN:
                    familyController.deleteAllChildrenOlderThan();
                    break;
                case EXIT:
                    cont = false;
                    break;
                default:
                    console.printLn("WRONG ITEM SELECTED. SELECT 1-10");
                    break;
            }
        }
    }
}
