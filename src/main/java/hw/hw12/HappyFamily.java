package hw.hw12;

import hw.hw12.console.SystemConsole;

public class HappyFamily {
    public static void main(String[] args)  {
        SystemConsole console = new SystemConsole();
        Core app = new Core(console);
        app.run();
    }
}
