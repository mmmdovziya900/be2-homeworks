package hw.hw12.service;

import hw.hw12.console.Console;
import hw.hw12.dao.FamilyDAO;
import hw.hw12.entity.*;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService {
    private final FamilyDAO familyDAO;
    private final Console console;

    public FamilyService(Console console, FamilyDAO familyDAO) {
        this.familyDAO = familyDAO;
        this.console = console;
    }


    public List<Family> getAllFamilies() {
        return familyDAO.getAllFamilies();
    }

    public void displayAllFamilies() {
        getAllFamilies().forEach(console::printLn);
    }

    public List<Family> getFamiliesBiggerThan(int numOfPeople) {
        List<Family> selected = getAllFamilies().stream().filter(family -> family.countFamily() > numOfPeople).collect(Collectors.toList());
        if (selected.size() != 0) {
            selected.forEach(console::printLn);
        } else {
            console.printLn("0");
        }
        return selected;
    }

    public List<Family> getFamiliesLessThan(int numOfPeople) {
        List<Family> selected = getAllFamilies().stream().filter(family -> family.countFamily() < numOfPeople).collect(Collectors.toList());
        if (selected.size() != 0) {
            selected.forEach(console::printLn);
        } else {
            console.printLn("0");
        }
        return selected;
    }

    public int countFamiliesWithMemberNumber() {
        return getAllFamilies().stream().mapToInt(Family::countFamily).sum();
    }

    public void createNewFamily(Human mother, Human father) {
        familyDAO.saveFamily(new Family(mother, father));
        console.printLn("SUCCESSFULLY CREATED!");
    }

    public boolean deleteFamilyByIndex(int index) {
        boolean b = familyDAO.deleteFamily(index);
        if (b) {
            console.printLn("SUCCESSFULLY DELETED!");
        } else {
            console.printLn("WRONG INDEX");
        }
        return b;
    }

    public Family bornChild(Family family, String masculine, String feminine) {
        if (family == null) {
            console.printLn("WRONG FAMILY INDEX!");
        } else if (masculine.equals("")) {
            family.getChildren().add(new Woman(feminine, family.getFather().getSurname(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "", 0));
        } else {
            family.getChildren().add(new Man(masculine, family.getFather().getSurname(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "", 0));
        }
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        if (family == null) {
            console.printLn("WRONG FAMILY INDEX!");
        } else {
            family.getChildren().add(child);
            familyDAO.saveFamily(family);
        }
        return family;
    }

    public void deleteAllChildrenOlderThan(int age) {
        for (Family family : getAllFamilies()) {
            List<Human> collected = family.getChildren().stream()
                    .filter(child -> Year.now().getValue() - Instant.ofEpochMilli(child.getBirthDate()).atZone(ZoneId.systemDefault()).toLocalDate().getYear() > age).collect(Collectors.toList());
            collected.forEach(family::deleteChild);
        }
    }

    public int count() {
        return getAllFamilies().size();
    }

    public Family getFamilyById(int index) {

        return familyDAO.getFamilyByIndex(index);
    }

    public List<Pet> getPets(int index) {
        return new ArrayList<>(getFamilyById(index).getPet());
    }

    public void addPet(int index, Pet pet) {
        Family f = getFamilyById(index);
        f.getPet().add(pet);
        familyDAO.saveFamily(f);
    }

    public void countWithNumberOfMember(int numOfMembers) {
        List<Family> selected = getAllFamilies().stream().filter(family -> family.countFamily() == numOfMembers).collect(Collectors.toList());
        console.printLn(selected.size() + "");
    }
}
