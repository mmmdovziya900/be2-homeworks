package hw.hw12;

public class EditMenu {
    public String show() {
        StringBuilder sb = new StringBuilder();
        String line = "-----------------------------------------------------|\n";
        sb.append("=====================================================\n")
                .append("|                 EDIT FAMILY                       |\n")
                .append("=====================================================|\n")
                .append(pad("1. GIVE BIRTH TO A BABY  "))
                .append(line)
                .append(pad("2. ADOPT A CHILD"))
                .append(line)
                .append(pad("3. RETURN TO A MAIN MENU "))
                .append(line);
        return sb.toString();
    }

    private String pad(String str) {
        return String.format("|" + "%-" + 52 + "s" + "|" + "\n", str);
    }
}
