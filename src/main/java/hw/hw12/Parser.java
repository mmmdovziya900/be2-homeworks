package hw.hw12;

public class Parser {
    public Command parse(String origin) {
        switch (origin.toLowerCase()) {
            case "1":
                return Command.TEST;
            case "2":
                return Command.DISPLAY_FAMILIES;
            case "3":
                return Command.DISPLAY_FAMILIES_MORE_THAN;
            case "4":
                return Command.DISPLAY_FAMILIES_lESS_THAN;
            case "5":
                return Command.COUNT_FAMILIES_OF_NUMBER;
            case "6":
                return Command.ADD_FAMILY;
            case "7":
                return Command.DELETE_FAMILY;
            case "8":
                return Command.EDIT_FAMILY;
            case "9":
                return Command.REMOVE_ALL_CHILDREN_OVER_THAN;
            case "10":
                return Command.EXIT;
            default:
                return Command.WRONG;
        }
    }
}