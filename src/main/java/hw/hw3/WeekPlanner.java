package hw.hw3;

import java.util.Scanner;

public class WeekPlanner {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String[][] schedule;
        boolean flag = false;
        schedule = new String[7][2];
        schedule[0][0] = "sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "tuesday";
        schedule[2][1] = "go in for sport";
        schedule[3][0] = "wednesday";
        schedule[3][1] = "clean up home";
        schedule[4][0] = "thursday";
        schedule[4][1] = "meet friend";
        schedule[5][0] = "friday";
        schedule[5][1] = "go to theatre";
        schedule[6][0] = "saturday";
        schedule[6][1] = "you are free today, do what you want";
        String day;
        System.out.println("Please, input the day of the week:");
        while (!flag) {
            day = in.nextLine();
            day = day.trim();
            if ((day.split(" ")[0].equals("change") || day.split(" ")[0].equals("reschedule")) && day.split(" ").length > 1) {
                String[] reSchedule = day.split(" ");
                day = reSchedule[1];
                day = day.toLowerCase();
                reschedule(day, schedule);
            }
            day = day.toLowerCase();
            switch (day) {
                case "sunday":
                    check(day, schedule);
                    break;
                case "monday":
                    check(day, schedule);
                    break;
                case "tuesday":
                    check(day, schedule);
                    break;
                case "wednesday":
                    check(day, schedule);
                    break;
                case "thursday":
                    check(day, schedule);
                    break;
                case "friday":
                    check(day, schedule);
                    break;
                case "saturday":
                    check(day, schedule);
                    break;
                case "exit":
                    flag = true;
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }

    public static void check(String day, String[][] schedule) {
        for (int i = 0; i < schedule.length; i++) {
            if (day.equals(schedule[i][0])) {
                System.out.println("Your tasks for " + schedule[i][0] + ":" + schedule[i][1]);
                System.out.println("Please, input the day of the week:");
                break;
            }
        }
    }

    public static void reschedule(String day, String[][] schedule) {
        boolean flag = false;
        for (int i = 0; i < schedule.length; i++) {
            if (day.equals(schedule[i][0])) {
                flag = true;
                Scanner scanner = new Scanner(System.in);
                System.out.println("Please, input new tasks for " + schedule[i][0] + " :");
                String task = scanner.nextLine();
                schedule[i][1] = task;
                System.out.println("Please, input the day of the week:");
                break;
            }
        }
        if (!flag) {
            System.out.println("Sorry, I don't understand you, please try again.");
        }

    }
}

