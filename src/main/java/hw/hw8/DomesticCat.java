package hw.hw8;

import java.util.Set;

public class DomesticCat extends Pet {
    private Species species = Species.DOMESTIC_CAT;

    public DomesticCat(String nickname) {
        super(nickname);
        super.setSpecies(this.species);
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(this.species);
    }

    public DomesticCat() {
        super();
        super.setSpecies(this.species);
    }

    @Override
    public void respond() {
        System.out.println("I am wild");
    }
}
