package hw.hw8;

import java.util.Set;

public class Dog extends Pet {
    private Species species = Species.DOG;

    public Dog(String nickname) {
        super(nickname);
        super.setSpecies(this.species);
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(this.species);
    }

    public Dog() {
        super();
        super.setSpecies(this.species);
    }

    @Override
    public void respond() {
        System.out.println("I am wild");
    }
}
