package hw.hw8;

import java.util.*;

public class HappyFamily {
    public static void main(String[] args) {
        Map<Enum, String> schedule = new HashMap<>();
        schedule.put(Day.SUNDAY, "do home work");
        schedule.put(Day.MONDAY, "go to courses; watch a film");
        schedule.put(Day.TUESDAY, "go in for sport");
        schedule.put(Day.WEDNESDAY, "clean up home");
        schedule.put(Day.THURSDAY, "meet friend");
        schedule.put(Day.FRIDAY, "go to theatre");
        schedule.put(Day.SATURDAY, "you are free today, do what you want");
        Set<String> habits = new HashSet<>();
        habits.add("Jump");
        habits.add("Dance");
        Dog dog = new Dog("Molly", 5, 86, habits);
        Pet fish = new Fish("Memo", 3, 86, habits);
        Human mother = new Human("July", "King", 1955, 75);
        Human father = new Human("Brad", "Pit", 1946, 90);
        Human child = new Human("Stephen", "King", 1989, 75);
        Man man = new Man("Jhonny", "King", 1989, 75);
        Woman woman = new Woman("Angelina", "King", 1955, 75);
        Family family = new Family(mother, father);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(fish);

        child.setFamily(family);
        System.out.println(father.equals(mother));
        family.addChild(child);
        family.addChild(child);
        System.out.println(family.deleteChild(child));
        family.setPet(pets);
        System.out.println(mother);
        System.out.println(father);
        System.out.println(pets);
        System.out.println(family);
        System.out.println(man);
        System.out.println(woman);
        System.out.println(family.countFamily());

        for (int i = 0; i < 1000000; i++) {
            new Human();
            if (i > 10000) {
                System.gc();
            }
        }
    }

    public enum Day {
        MONDAY, TUESDAY,
        WEDNESDAY, THURSDAY,
        FRIDAY, SATURDAY, SUNDAY
    }
}
