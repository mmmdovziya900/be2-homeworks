package hw.hw8;

import java.util.Map;

public class Man extends Human {

    public Man(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Man(String name, String surname, int year, int iq, Map<Enum, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man() {
        super();
    }

    public void repairCar() {
        System.out.println(super.getName() + " is repairing his car");
    }

    public String toString() {
        String sh = "";
        if (super.getSchedule() != null) {
            for (Map.Entry<Enum, String> entry : super.getSchedule().entrySet()) {
                sh += "[" + entry.getKey() + ", " + entry.getValue() + "]";
            }
        }
        String s = "Man{name='" + super.getName() + "', surname='" + super.getSurname() + "', year=" + super.getYear() + ", iq=" + super.getIq() + ", schedule=[" + sh + "]}";
        return s;
    }
}
