package hw.hw8;

public enum Species {
    DOG, DOMESTIC_CAT, FISH, ROBO_CAT, UNKNOWN
}
