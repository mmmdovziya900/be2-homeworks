package hw.hw9.entity;

import java.util.Map;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Map<Enum, String> schedule;
    private Family family;

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    public Human(String name, String surname, int year, int iq, Map<Enum, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human() {

    }

    public void greetPet() {
        family.getPet().forEach(pet -> System.out.println("Hello, " + pet.getNickname()));
    }

    public void describePet() {
        family.getPet().forEach(pet -> System.out.println("I have a " + pet.getSpecies() + ", he is " + pet.getAge() + " years old, " + (pet.getTrickLevel() > 50 ? "he is very sly" : "he is almost not sly")));
        System.out.println();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<Enum, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<Enum, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Finalize method called in Human class");
        super.finalize();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getYear() == human.getYear() &&
                Objects.equals(getName(), human.getName()) &&
                Objects.equals(getSurname(), human.getSurname());
    }

    @Override
    public String toString() {
        String sh = "";
        if (schedule != null) {
            for (Map.Entry<Enum, String> entry : schedule.entrySet()) {
                sh += "[" + entry.getKey() + ", " + entry.getValue() + "]";
            }
        }
        String s = "Human{name='" + name + "', surname='" + surname + "', year=" + year + ", iq=" + iq + ", schedule=[" + sh + "]}";
        return s;
    }

}
