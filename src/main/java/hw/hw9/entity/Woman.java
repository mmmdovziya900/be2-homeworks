package hw.hw9.entity;

import java.util.Map;

public class Woman extends Human {
    public Woman(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Woman(String name, String surname, int year, int iq, Map<Enum, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman() {
        super();
    }

    public void makeUp() {
        System.out.println(super.getName() + " is doing make-up");
    }

    public String toString() {
        String sh = "";
        if (super.getSchedule() != null) {
            for (Map.Entry<Enum, String> entry : super.getSchedule().entrySet()) {
                sh += "[" + entry.getKey() + ", " + entry.getValue() + "]";
            }
        }
        String s = "Woman{name='" + super.getName() + "', surname='" + super.getSurname() + "', year=" + super.getYear() + ", iq=" + super.getIq() + ", schedule=[" + sh + "]}";
        return s;
    }
}