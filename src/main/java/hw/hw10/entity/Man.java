package hw.hw10.entity;

import java.util.Map;

public class Man extends Human {

    public Man(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public Man(String name, String surname, String birthDate, int iq, Map<Enum, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public void repairCar() {
        System.out.println(super.getName() + " is repairing his car");
    }

    public String toString() {
        String sh = "";
        if (super.getSchedule() != null) {
            for (Map.Entry<Enum, String> entry : super.getSchedule().entrySet()) {
                sh += "[" + entry.getKey() + ", " + entry.getValue() + "]";
            }
        }
        String s = "Man{name='" + super.getName() + "', surname='" + super.getSurname() + ", birth date=" + super.toStringFormatted(super.getBirthDate()) + ", iq=" + super.getIq() + ", schedule=[" + sh + "]}";
        return s;
    }
}
