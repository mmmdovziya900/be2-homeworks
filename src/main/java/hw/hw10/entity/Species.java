package hw.hw10.entity;

public enum Species {
    DOG, DOMESTIC_CAT, FISH, ROBO_CAT, UNKNOWN
}
