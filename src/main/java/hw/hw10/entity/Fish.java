package hw.hw10.entity;

import java.util.Set;

public class Fish extends Pet {
    private Species species = Species.FISH;
    public Fish(String nickname) {
        super(nickname);
        super.setSpecies(this.species);
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(this.species);
    }

    public Fish() {
        super();
        super.setSpecies(this.species);
    }

    @Override
    public void respond() {
        System.out.println("I am wild");
    }
}
