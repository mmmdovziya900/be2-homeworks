package hw.hw4;

public class HappyFamily {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go in for sport";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "clean up home";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "meet friend";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go to theatre";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "you are free today, do what you want";
        String[] habits = {"Dance", "Jump"};

        Pet pet = new Pet("Dog", "Molly", 5, 86, habits);
        Pet pet1 = new Pet("dog", "Rosie");
        Pet pet2 = new Pet();
        pet1.eat();
        pet1.respond();
        pet1.foul();
        pet.eat();
        pet.respond();
        pet.foul();
        System.out.println(pet.toString());
        System.out.println(pet1.toString());
        System.out.println(pet2.toString());

        Human human = new Human();
        Human mother = new Human("Sevil", "Aliyeva", 1985);
        Human father = new Human("Adil", "Aliyev", 1982);
        Human human1 = new Human("Ziya", "Mammadov", 2000, father, mother);
        Human child = new Human("Ziya", "Mammadov", 2000, 90, pet, father, mother, schedule);
        System.out.println(mother.toString());
        System.out.println(father.toString());
        System.out.println(human1.toString());
        System.out.println(child.toString());
        child.pet.eat();
        child.pet.foul();
        child.pet.respond();
        child.describePet();
        child.greetPet();
        human1.describePet();
        human1.greetPet();

    }
}
