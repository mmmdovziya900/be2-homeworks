package hw.hw4;

public class Human {
    public String name;
    public String surname;
    public int year;
    public int iq;
    public Pet pet;
    public Human mother;
    public Human father;
    public String[][] schedule;

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = new Human();
        this.father = new Human();
        this.pet = new Pet();
    }

    public Human(String name, String surname, int year, Human father, Human mother) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
        this.pet = new Pet();
    }

    public Human(String name, String surname, int year, int iq, Pet pet, Human father, Human mother, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.father = father;
        this.mother = mother;
        this.schedule = schedule;
    }

    public Human() {

    }

    public void greetPet() {
        System.out.println("Hello, " + pet.nickname);
    }

    public void describePet() {
        System.out.println("I have a " + pet.species + ", he is " + pet.age + " years old, " + (pet.trickLevel > 50 ? "he is very sly" : "he is almost not sly"));
    }

    @Override
    public String toString() {
        String s = "Human{name='" + name + "', surname='" + surname + "', year=" + year + ", iq=" + iq + (mother.name != null ? ", mother=" + mother.name + " " + mother.surname : "") + (father.name != null ? ", father=" + father.name + " " + father.surname : "") + (pet.toString().equals("") ? "" : ", ") + pet.toString() + "}";
        return s;
    }
}
