package hw.hw6;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
        this.pet = new Pet();
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public void greetPet() {
        System.out.println("Hello, " + pet.getNickname());
    }

    public void describePet() {
        System.out.println("I have a " + pet.getSpecies() + ", he is " + pet.getAge() + " years old, " + (pet.getTrickLevel() > 50 ? "he is very sly" : "he is almost not sly"));
    }

    public void addChild(Human child) {
        Human[] ar = new Human[children.length + 1];
        for (int i = 0; i < children.length; i++) {
            ar[i] = children[i];
        }
        ar[children.length] = child;
        this.children = ar;
    }

    public boolean deleteChild(int n) {
        if (children.length != 0) {
            Human[] ar = new Human[children.length - 1];
            if (n < children.length) {
                for (int i = 0; i < n; i++) {
                    ar[i] = children[i];
                }
                for (int i = n + 1; i < children.length; i++) {
                    ar[i - 1] = children[i];
                }
                this.children = ar;
                return true;
            } else {
                System.out.println("No child at this index");
                return false;
            }
        } else {
            return false;
        }

    }

    public boolean deleteChild(Human child) {
        for (int j = 0; j < children.length; j++) {
            if (children[j] == child) {
                deleteChild(j);
                break;
            }
        }
        return false;
    }

    public int countFamily() {
        return 2 + children.length;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Finalize method called in Family class");
        super.finalize();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Family))
            return false;
        if (obj == this)
            return true;
        return this.getMother().getName() == ((Family) obj).getMother().getName() && this.getFather().getName() == ((Family) obj).getFather().getName();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        String[] childrenInfo = {""};
        for (Human child : children) {
            childrenInfo[0] += child.toString() + ", ";
        }
        return father.toString() + ", " + mother.toString() + ", " + childrenInfo[0] + pet.toString();
    }
}
