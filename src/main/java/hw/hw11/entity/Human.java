package hw.hw11.entity;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<Enum, String> schedule;
    private Family family;


    public Human(String name, String surname, String birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = toEpochMilli(birthDate);
        this.iq = iq;
    }


    public Human(String name, String surname, String birthDate, int iq, Map<Enum, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = toEpochMilli(birthDate);
        this.iq = iq;
        this.schedule = schedule;
    }

    public void greetPet() {
        family.getPet().forEach(pet -> System.out.println("Hello, " + pet.getNickname()));
    }

    public void describePet() {
        family.getPet().forEach(pet -> System.out.println("I have a " + pet.getSpecies() + ", he is " + pet.getAge() + " years old, " + (pet.getTrickLevel() > 50 ? "he is very sly" : "he is almost not sly")));
        System.out.println();
    }

    public String describeAge() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(birthDate), TimeZone
                .getDefault().toZoneId());
        LocalDateTime now = LocalDateTime.now();
        String birth = formatter.format(dateTime);
        String year = birth.split("/")[2];
        String month = birth.split("/")[1];
        String day = birth.split("/")[1];
        return (now.getYear() - Integer.parseInt(year)) + " years, "
                + (now.getMonthValue() - Integer.parseInt(month)) + " months, "
                + (now.getDayOfMonth() - Integer.parseInt(day) + " days past");
    }

    public long toEpochMilli(String birthDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        long l = 0;
        try {
            l = LocalDate.parse(birthDate, formatter).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return l;
    }

    public String toStringFormatted(long l) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate dateTime = Instant.ofEpochMilli(l).atZone(ZoneId.systemDefault()).toLocalDate();
        return formatter.format(dateTime);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<Enum, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<Enum, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Finalize method called in Human class");
        super.finalize();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getBirthDate() == human.getBirthDate() &&
                Objects.equals(getName(), human.getName()) &&
                Objects.equals(getSurname(), human.getSurname());
    }

    @Override
    public String toString() {
        String sh = "";
        if (schedule != null) {
            for (Map.Entry<Enum, String> entry : schedule.entrySet()) {
                sh += "[" + entry.getKey() + ", " + entry.getValue() + "]";
            }
        }
        String s = "Human{name='" + name + "', surname='" + surname + "', birth date=" + toStringFormatted(birthDate) + ", iq=" + iq + ", schedule=[" + sh + "]}";
        return s;
    }

}
