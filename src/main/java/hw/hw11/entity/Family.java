package hw.hw11.entity;

import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<>(0);
        this.pets = new HashSet<>();
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Set<Pet> getPet() {
        return pets;
    }

    public void setPet(Set<Pet> pets) {
        this.pets = pets;
    }

    public List<Human> getChildren() {
        return children;
    }


    public void addChild(Human child) {
        children.add(child);
    }

    public boolean deleteChild(int n) {
        int size = children.size();
        if (size != 0 && n < size) {
            children.remove(n);
            return true;
        } else {
            return false;
        }

    }

    public boolean deleteChild(Human child) {
        int size = children.size();
        if (size != 0 && children.indexOf(child) < size) {
            children.remove(child);
            return true;
        } else {
            return false;
        }
    }

    public int countFamily() {
        return 2 + children.size();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Finalize method called in Family class");
        super.finalize();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return Objects.equals(getMother(), family.getMother()) &&
                Objects.equals(getFather(), family.getFather()) ;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return  father + ", " + mother + ", " + children + ", " + pets;
    }
}
