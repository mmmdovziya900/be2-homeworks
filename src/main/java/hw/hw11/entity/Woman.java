package hw.hw11.entity;

import java.util.Map;

public class Woman extends Human {
    public Woman(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public Woman(String name, String surname, String birthDate, int iq, Map<Enum, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public void makeUp() {
        System.out.println(super.getName() + " is doing make-up");
    }

    public String toString() {
        String sh = "";
        if (super.getSchedule() != null) {
            for (Map.Entry<Enum, String> entry : super.getSchedule().entrySet()) {
                sh += "[" + entry.getKey() + ", " + entry.getValue() + "]";
            }
        }
        String s = "Woman{name='" + super.getName() + "', surname='" + super.getSurname() + ", birth date=" + super.toStringFormatted(super.getBirthDate())+ ", iq=" + super.getIq() + ", schedule=[" + sh + "]}";
        return s;
    }
}