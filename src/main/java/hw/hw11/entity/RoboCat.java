package hw.hw11.entity;

import java.util.Set;

public class RoboCat extends Pet {
    private Species species = Species.ROBO_CAT;
    public RoboCat(String nickname) {
        super(nickname);
        super.setSpecies(this.species);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(this.species);
    }

    public RoboCat() {
        super();
        super.setSpecies(this.species);
    }

    @Override
    public void respond() {
        System.out.println("I am wild");
    }
}
