package hw.hw11.service;

import hw.hw11.dao.FamilyDAO;
import hw.hw11.entity.Family;
import hw.hw11.entity.Human;
import hw.hw11.entity.Pet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService {
    private final FamilyDAO familyDAO;

    public FamilyService(FamilyDAO familyDAO) {
        this.familyDAO = familyDAO;
    }


    public List<Family> getAllFamilies() {
        return familyDAO.getAllFamilies();
    }

    public void displayAllFamilies() {
        getAllFamilies().forEach(System.out::println);
    }

    public List<Family> getFamiliesBiggerThan(int numOfPeople) {
        List<Family> selected = getAllFamilies().stream().filter(family -> family.countFamily() > numOfPeople).collect(Collectors.toList());
        selected.forEach(System.out::println);
        return selected;
    }

    public List<Family> getFamiliesLessThan(int numOfPeople) {
        List<Family> selected = getAllFamilies().stream().filter(family -> family.countFamily() < numOfPeople).collect(Collectors.toList());
        selected.forEach(System.out::println);
        return selected;
    }

    public int countFamiliesWithMemberNumber() {
        return getAllFamilies().stream().mapToInt(Family::countFamily).sum();
    }

    public void createNewFamily(Human father, Human mother) {
        familyDAO.saveFamily(new Family(mother, father));
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDAO.deleteFamily(index);
    }

    public Family bornChild(Family family, String masculine, String feminine) {
        if (masculine.equals("")) {
            family.getChildren().add(new Human(feminine, family.getFather().getSurname(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+"", 0));
        } else {
            family.getChildren().add(new Human(masculine, family.getFather().getSurname(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+"", 0));
        }
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.getChildren().add(child);
        familyDAO.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThan(int age) {
        for (Family family : getAllFamilies()) {
            List<Human> collected = family.getChildren().stream()
                    .filter(child -> Year.now().getValue() -Instant.ofEpochMilli(child.getBirthDate()).atZone(ZoneId.systemDefault()).toLocalDate().getYear()  > age).collect(Collectors.toList());
            collected.forEach(family::deleteChild);
        }
    }

    public int count() {
        return getAllFamilies().size();
    }

    public Family getFamilyById(int index) {

        return familyDAO.getFamilyByIndex(index);
    }

    public List<Pet> getPets(int index) {
        return new ArrayList<>(getFamilyById(index).getPet());
    }

    public void addPet(int index, Pet pet) {
        Family f = getFamilyById(index);
        f.getPet().add(pet);
        familyDAO.saveFamily(f);
    }
}
