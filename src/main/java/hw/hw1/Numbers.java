package hw.hw1;

import java.util.Random;
import java.util.Scanner;

class Numbers {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int randomNum = random.nextInt(100);
        System.out.println("Enter a name: ");
        String name = scanner.next();
        System.out.println("Let  the game begin!");
        int number;
        do {
            System.out.println("Enter a number: ");
            number = scanner.nextInt();
            if (number < randomNum) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (number > randomNum) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Congratulations, " + name + "!");
            }
        } while (number != randomNum);
    }
}
