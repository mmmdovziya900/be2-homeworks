package hw.hw2;

import java.util.Random;
import java.util.Scanner;

public class ShootingAtTheSquare {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random r = new Random();
        boolean check = false;
        int targetX = 1 + r.nextInt(4);
        int targetY = 1 + r.nextInt(4);
        char[][] map = new char[6][6];
        buildMap(map);
        System.out.println("All set. Get ready to rumble!");

        while (!check) {
            System.out.println("Please, enter a horizontal point for fire");
            int playerX = in.nextInt();
            while (playerX <= 0 || playerX > 5) {
                System.out.println("Try again, enter a number between 1-5!");
                playerX = in.nextInt();
            }
            System.out.println("Please, enter a vertical point for fire");
            int playerY = in.nextInt();
            while (playerY <= 0 || playerY > 5) {
                System.out.println("Try again, enter a number between 1-5!");
                playerY = in.nextInt();
            }
            check = checkSuccess(map, playerX, playerY, targetX, targetY);
        }

    }

    public static void buildMap(char[][] map) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map.length; j++) {
                map[0][i] = (char) (i + '0');
                map[i][0] = (char) (i + '0');
                if (i + 1 < map.length && j + 1 < map.length) {
                    map[i + 1][j + 1] = '-';
                }
            }
        }
    }

    public static void displayMap(char[][] map) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map.length; j++) {
                System.out.print(map[i][j] + " | ");
            }
            System.out.println("");
        }
    }

    public static boolean checkSuccess(char[][] map, int playerX, int playerY, int targetX, int targetY) {
        if (playerX == targetX && playerY == targetY) {
            map[playerY][playerX] = 'x';
            System.out.println("You have won!");
            displayMap(map);
            return true;

        } else {
            map[playerY][playerX] = '*';
            displayMap(map);
            return false;
        }

    }
}
