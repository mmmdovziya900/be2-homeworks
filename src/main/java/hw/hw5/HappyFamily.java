package hw.hw5;

public class HappyFamily {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go in for sport";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "clean up home";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "meet friend";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go to theatre";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "you are free today, do what you want";
        String[] habits = {"Dance", "Jump"};

        Pet pet = new Pet("Dog", "Molly", 5, 86, habits);
        Pet pet1 = new Pet("dog", "Rosie");
        Pet pet2 = new Pet();

        Human mother = new Human("July", "King", 1955, 75, schedule);
        Human father = new Human("Brad", "Pit", 1946, 90, schedule);
        Human child = new Human("Stephen", "King", 1989, 75, schedule);
        Family family = new Family(mother, father);

        System.out.println(pet.equals(pet1));
        System.out.println(father.equals(mother));
        family.addChild(child);
        family.addChild(child);
        System.out.println(family.deleteChild(child));
        family.setPet(pet);
        family.getPet().foul();
        family.getPet().respond();
        family.getPet().eat();
        family.greetPet();
        family.describePet();

        System.out.println(family.toString());
        System.out.println(family.countFamily());
        System.out.println(father.toString());

        pet.eat();
        pet.respond();
        pet.foul();
        System.out.println(pet.toString());
    }
}
