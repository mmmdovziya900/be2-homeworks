package hw.hw6;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FamilyTest {
    Family f;

    Human mother = new Human("July", "King", 1955, 75);
    Human father = new Human("Brad", "Pit", 1946, 90);
    Human child = new Human("Stephen", "King", 1989, 75);

    @Before
    public void before999() {
        this.f = new Family(mother, father);
    }

    @Test
    public void addChild1() {
        f.addChild(child);
        assertEquals(1, f.getChildren().length);
    }

    @Test
    public void deleteChild() {
        f.addChild(child);
        f.deleteChild(0);
        assertEquals(0, f.getChildren().length);
    }

    @Test
    public void deleteChild1() {
        f.addChild(child);
        f.deleteChild(child);
        assertEquals(0, f.getChildren().length);
    }

    @Test
    public void countFamily() {
        f.addChild(child);
        f.addChild(child);
        assertEquals(4, f.countFamily());

    }
}