package hw.hw11.service;

import hw.hw11.controller.FamilyController;
import hw.hw11.dao.CollectionFamilyDao;
import hw.hw11.entity.*;
import hw.hw11.service.FamilyService;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class FamilyServiceTest {
    private FamilyController controller;
    private Pet dog;
    private Pet fish;
    private Pet domesticCat;
    private Pet cat;
    private Human father;
    private Human child;
    private Human child1;
    private Human man;
    private Human woman;

    @Before
    public void before() {
        this.controller = new FamilyController(new FamilyService(new CollectionFamilyDao()));
        Set<String> habits = new HashSet<>();
        habits.add("Jump");
        habits.add("Dance");
        this.dog = new Dog("Molly", 5, 86, habits);
        this.fish = new Fish("Memo", 3, 86, habits);
        this.domesticCat = new DomesticCat("Max", 3, 86, habits);
        this.cat = new RoboCat("Sam", 3, 86, habits);
        Human mother = new Woman("July", "King", "20/03/2016", 75);
        this.father = new Man("Brad", "Pit", "20/03/2016", 90);
        this.child = new Human("Stephen", "King", "20/03/2010", 75);
        this.child1 = new Human("Mark", "Jhonas", "22/03/2010", 75);
        this.man = new Human("Jonny", "King", "20/03/2016", 75);
        this.woman = new Human("Angelina", "King", "20/03/2016", 75);
        controller.createNewFamily(man, woman);
        controller.createNewFamily(father, mother);

    }

    @Test
    public void getAllFamilies() {
        assertEquals(2, controller.getAllFamilies().size());
    }

    @Test
    public void getFamiliesBiggerThan() {
        Family f = new Family(man, woman);
        controller.adoptChild(f, child);
        controller.adoptChild(f, child);
        assertEquals(1, controller.getFamiliesBiggerThan(2).size());
    }

    @Test
    public void getFamiliesLessThan() {
        Family f = new Family(man, woman);
        controller.adoptChild(f, child);
        controller.adoptChild(f, child);
        assertEquals(2, controller.getFamiliesLessThan(3).size());
    }

    @Test
    public void countFamiliesWithMemberNumber() {
        assertEquals(4, controller.countFamiliesWithMemberNumber());
    }

    @Test
    public void createNewFamily() {
        controller.createNewFamily(man, father);
        assertEquals(3, controller.count());
    }

    @Test
    public void deleteFamilyByIndex() {
        controller.deleteFamilyByIndex(0);
        assertEquals(1, controller.count());
    }

    @Test
    public void bornChild() {
        Family f = controller.getFamilyById(1);
        controller.bornChild(f, "Keanu", "");
        assertEquals(1, f.getChildren().size());
    }

    @Test
    public void adoptChild() {
        Family f = controller.getFamilyById(1);
        controller.adoptChild(f, child);
        assertEquals(1, f.getChildren().size());
    }

    @Test
    public void deleteAllChildrenOlderThan() {
        Family f = controller.getFamilyById(1);
        controller.adoptChild(f, child);
        controller.adoptChild(f,child1);
        controller.deleteAllChildrenOlderThan(5);
        assertEquals(0, controller.getFamilyById(1).getChildren().size());
    }

    @Test
    public void count() {
        assertEquals(2, controller.count());
    }

    @Test
    public void getFamilyById() {
        assertNotNull(controller.getFamilyById(0));

    }

    @Test
    public void getPets() {
        controller.addPet(1, dog);
        controller.addPet(1, cat);
        controller.addPet(1, domesticCat);
        assertEquals(3, controller.getPets(1).size());

    }

    @Test
    public void addPet() {
        controller.addPet(1, domesticCat);
        assertEquals(1, controller.getFamilyById(1).getPet().size());
    }
}
